import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import AboutView from '../views/AboutView.vue'
import TeamView from '../views/TeamView.vue'
import TeamDetailsView from '../views/TeamDetailsView.vue'
import PlayerView from '../views/PlayerView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
      component: () => AboutView
    },
    {
      path: '/teams',
      name: 'teams',
      component: () => TeamView
    },
    {
      path: '/teams/:teamId',
      name: 'teamDetails',
      component: () => TeamDetailsView
    },
    {
      path: '/teams/:teamId/players/:playerId',
      name: 'players',
      component: () => PlayerView
    }

  ]
})

export default router
