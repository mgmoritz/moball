import type { Player } from '@/types/Player'

export async function fetchTeamPlayers(teamId: string): Promise<Player[]> {
  const response = await fetch(`http://localhost:3000/teams/${teamId}/players/`)
  return response.json()
}
export async function fetchPlayerDetails(teamId: string, playerId: string): Promise<Player> {
  const response = await fetch(`http://localhost:3000/teams/${teamId}/players/${playerId}`)
  return response.json()
}
