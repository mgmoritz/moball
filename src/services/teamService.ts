import type { Team } from '@/types/Team'
import type { Player } from '@/types/Player'
import type { Event } from '@/types/Event'

export async function fetchTeams(): Promise<Team[]> {
  const response = await fetch(`http://localhost:3000/teams`)
  return response.json()
}
export async function fetchTeamDetails(teamId: string): Promise<Team> {
  const response = await fetch(`http://localhost:3000/teams/${teamId}`)
  return response.json()
}
export async function fetchTeamPlayers(teamId: string): Promise<Player[]> {
  const response = await fetch(`http://localhost:3000/teams/${teamId}/players`)
  return response.json()
}
export async function fetchTeamEvents(teamId: string): Promise<Event[]> {
  const response = await fetch(`http://localhost:3000/teams/${teamId}/events`)
  return response.json()
}
