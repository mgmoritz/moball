import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { Player } from '@/types/Player';

export const usePlayerStore = defineStore('player', () => {
  const player = ref()
  function set(p: Player) {
    player.value = p
  }
  return { player, set }
})
