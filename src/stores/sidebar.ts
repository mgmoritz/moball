import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useSidebarStore = defineStore('sidebar', () => {
  const sidebar = ref(true)

  function toggleSidebar() {
    sidebar.value = !sidebar.value
  }

  return { sidebar, toggleSidebar }
})
