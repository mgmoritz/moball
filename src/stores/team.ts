import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { Team } from '@/types/Team';

export const useTeamStore = defineStore('player', () => {
  const team = ref()
  function set(t: Team) {
    team.value = t
  }
  return { team, set }
})
