export interface Event {
  id: string;
  name: string;
  date: date;
}
