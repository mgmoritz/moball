export interface Team {
  id: string;
  name: string;
  emblemUrl: string;
  fairPlayPoints: number;
  commitmentPoints: number;
}
