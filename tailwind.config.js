export default {
  content: ['./src/**/*.{js,jsx,ts,tsx,vue}', './index.html'], // Update the purge option to content
  darkMode: 'media',
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
};